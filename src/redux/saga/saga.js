import { loadUserData, requestUserData,errorData } from "../action/action";
import { put, takeEvery, call } from "redux-saga/effects";

export function* fetchingSaga() {
  try {
    yield put(requestUserData());

   const data= yield call(async() => {
      return fetch("https://jsonplaceholder.typicode.com/comments").then(res => res.json());
      
    });
    // console.log("the data is --",data)
    yield put(loadUserData(data));
  } catch (error) {
    yield put(errorData("This error is occured in try-catch", error));
  }
}

export function* watcherSaga() {
 yield takeEvery("REQUESTING_DATA", fetchingSaga);
}
