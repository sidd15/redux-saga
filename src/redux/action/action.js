import {
  REQUESTING_DATA,
  USERS_REQUESTED,
  USERS_SUCCEEDED,
  USER_REQUEST_FAILED,
} from "./actionTypes";
 
export function loadUserData(data) {
  return {
    type: USERS_SUCCEEDED,
    payload: {
      data,
    },
  };
}

export function requestUserData() {
  return {
    type: USERS_REQUESTED,
  };
}

export function errorData(error) {
  return {
    type: USER_REQUEST_FAILED,
    payload: {
      error,
    },
  };
}

export function requestingData() {
  return {
    type: REQUESTING_DATA,
  };
}
