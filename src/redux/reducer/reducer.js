import {
  USERS_REQUESTED,
  USERS_SUCCEEDED,
  USER_REQUEST_FAILED,
} from "../action/actionTypes";

const initialState = {
  loading: true,
  data: [],
  error: [],
};
const arr=["sid","mike","john"]
export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USERS_REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case USERS_SUCCEEDED:
      return {
        ...state,
        loading:false,
        data: action.payload.data.map((elem) => {
          return {
            elem,
          };
        }),

        // data:arr.map((elem)=>{
        //     return {
        //          elem
        //     }
        // })
      };
    case USER_REQUEST_FAILED:
      return {
        ...state,
        error: "Error occured ...",
      };

    default:
      return state;
  }
};
