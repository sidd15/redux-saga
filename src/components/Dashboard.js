import React from "react";
import { useEffect ,memo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { requestingData } from "../redux/action/action";

function Dashboard() {
  const response = useSelector((state) => {
    return state;
  });

  console.log(response)
  
  const dispatch = useDispatch();
  useEffect(()=>{
      dispatch(requestingData())
  },[])
  
  return (
      <div>
      <h1>This is redux-saga</h1>
      {}
    </div>
  );
}

export default memo(Dashboard);
// useEffect(()=>{
//     const fetchData=async()=>{
//         const resp= await fetch("https://jsonplaceholder.typicode.com/comments")
//         const data=await resp.json()
//         console.log(data)

//     }
//     fetchData()
// },[])
